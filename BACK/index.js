
const express = require('express');
const router = express.Router();
const server = express();
const path = require('path');
const hbs = require('hbs');
const passport = require('passport');
require('./passport'); // Requerimos nuestro archivo de configuración
const session = require('express-session');
const PORT = process.env.PORT || 3000;
const cors = require('cors');
const MongoStore = require('connect-mongo');
const mongoose = require('mongoose');
const { DB_URL} = require('./config/db');
const authMiddleware = require('./middlewares/auth.middleware'); //server.use('/pets', [authMiddleware.isAuthenticated], petRouter);

//Añadimos las opciones para habilitar las cors
server.use(cors());
//Añadimos las opciones para poder leer el body
server.use(express.json());
server.use(express.urlencoded({ extended: false }));

//Añadimos las opciones para crear las sesiones y para que registre las cookie
server.use(passport.initialize());
server.use(passport.session()); // Este middlware añadirá sesiones a nuestros usuarios

server.use(
  session({
    secret: process.env.SESSION_SECRET || 'upgradehub_node', // ¡Este secreto tendremos que cambiarlo en producción!
    resave: false, // Solo guardará la sesión si hay cambios en ella.
    saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
    cookie: {
      maxAge: 3600000, // Milisegundos de duración de nuestra cookie, en este caso será una hora.
    },
    store: MongoStore.create({
      mongoUrl: DB_URL,
    })
  })
);
server.use(passport.initialize());
server.use(passport.session());

//Añadimos las opciones para definir las vistas y la carpeta public
//server.set('views', path.join(__dirname, 'views'));

server.set('view engine', 'hbs');
server.use(express.static(path.join(__dirname, 'public')));

const homeRoutes = require('./routes/home.routes');
const userRoutes = require('./routes/user.routes');
const citaRoutes = require('./routes/cita.routes');
const medicoRoutes = require('./routes/medico.routes');

//Añadimos las opciones para tratar las rutas
//server.use('/citas', [authMiddleware.isAuthenticated], citaRoutes);
server.use('/', homeRoutes);
server.use('/user', userRoutes);
server.use('/medicos', medicoRoutes);
server.use('/citas', citaRoutes);

server.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});
module.exports = PORT;
