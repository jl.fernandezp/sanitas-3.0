const mongoose = require('mongoose');
const { Schema, model } = mongoose;


const citasSchema = new Schema(
  {
    pacienteId: { type: Schema.Types.ObjectId, ref: 'User'},
    medicoId: { type: Schema.Types.ObjectId, ref: 'Medico'},
    fecha: { type: Date, required: true},
    horaini: { type: String, required: true},
    horafin: { type: String, required: true},
    tipo: {type: String,
      enum: ['Presencial', 'Telefónica', 'Videoconsulta']
    }

  }, 
  {
    timestamps: true,
  }
);

const Citas = model('Citas', citasSchema);
module.exports = Citas;
