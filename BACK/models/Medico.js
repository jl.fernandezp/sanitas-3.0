const mongoose = require('mongoose');
const { Schema, model } = mongoose;

const medicosSchema = new Schema(
  {
    nombre: { type: String, required: true},
    especialidad: {type: String,
      enum: ['Cardiología', 'Pediatría', 'Medicina general', 'Oftalmología', 'Ginecología', 'Fisioterapia'], required: true},
  }, 
  {
    timestamps: true,
  }
);

const Medico = model('Medico', medicosSchema);
module.exports = Medico;
