const mongoose = require('mongoose');
const { Schema, model } = mongoose;

const userSchema = new Schema(
  {
    dni: { type: String, required: true, unique: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    fullname: { type: String, required: true }, //revisar requerido
    fecha: { type: Date},
    direccion: { type: String },
    profileImg: { type: String, default: "user-pic.jpg"},
  }, 
  {
    timestamps: true,
  }
);
const User = model('User', userSchema);
module.exports = User;
