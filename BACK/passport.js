const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('./models/User');
const saltRounds = 10;
const sendEmail = require('./utils/sendEmail');

// Creamos los salts de bcrypt
passport.use(
  'register',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        // Primero buscamos si el usuario existe en nuestra DB
        const previousUser = await User.findOne({ email: email });
        // Si hay usuario previamente, lanzamos un error
        if (previousUser) {
          const error = new Error('El usuario ya existe!');
          return done(error);
        }
        // Si no existe el usuario, vamos a "hashear" el password antes de registrarlo
        const hash = await bcrypt.hash(password, saltRounds);
        // Creamos el nuevo user y lo guardamos en la DB
        const newUser = new User({
          email: email,
          password: hash,
          dni: req.body.dni,
          fullname: req.body.fullname,
          fecha:req.body.fecha,
          direccion: req.body.direccion,
        });
        const savedUser = await newUser.save(); 
        const isEmialSend = await sendEmail(savedUser.email,"MediCal - Alta usuario",`Gracias ${fullname} por darte de alta`);     
        // Invocamos el callback con null donde iría el error y el usuario creado
        done(null, savedUser);
      } catch (err) {
        return done(err);
      }
    }
  )
);

//Creamos la funcion de login
passport.use(
  'login',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        // Primero buscamos si el usuario existe en nuestra DB
        const currentUser = await User.findOne({ email: email });

        // Si NO existe el usuario, tendremos un error...
        if (!currentUser) {
          const error = new Error('El usuario no existe!');
          return done(error);
        }
        // // Si existe el usuario, vamos a comprobar si su password enviado coincide con el registrado
        const isValidPassword = await bcrypt.compare(
          password,
          currentUser.password
        );
        // Si el password no es correcto, enviamos un error a nuestro usuario
        if (!isValidPassword) {
          const error = new Error(
            'El email o la contraseña no es correcta!'
          );
          return done(error);
        }
        // Si todo se valida correctamente, completamos el callback con el usuario
        done(null, currentUser);
      } catch (err) {
        // Si hay un error, resolvemos el callback con el error
        return done(err);
      }
    }
  )
);

// Esta función usará el usuario de req.LogIn para registrar su id en la cookie de sesión
passport.serializeUser((user, done) => {
  return done(null, user._id);
});

// Esta función buscará un usuario dada su _id en la DB y populará req.user si existe
passport.deserializeUser(async (userId, done) => {
  try {
    const existingUser = await User.findById(userId);
    return done(null, existingUser);
  } catch (err) {
    return done(err);
  }
});
