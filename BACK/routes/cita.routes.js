/* eslint-disable new-cap */
const express = require("express");
const passport = require("passport");
const router = express.Router();
const Citas = require("../models/Citas");
const fileMiddlewares = require("../middlewares/file.middleware");

// Para devolver formato json return res.status(200).json(publi);
router.get("/", async (req, res) => {
  usr = req.query.usr
  try {
    let citas = []
    if (usr?.length){
       citas = await Citas.find({pacienteId:usr}).populate('medicoId').populate('pacienteId');
    }
    else{
       citas = await Citas.find().populate('medicoId').populate('pacienteId');
    }
    return res.status(200).json(citas);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.post("/", async (req, res) => {
  try {
    const newCitas = new Citas({
      pacienteId: req.body.pacienteId,
      medicoId: req.body.medicoId,
      fecha: req.body.fecha,
      horaini: req.body.horaini,
      horafin: req.body.horafin,
      tipo: req.body.tipo,
    });
    const createdCitas = await newCitas.save();
    return res.status(200).json(createdCitas);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const citadetail = await Citas.findById(req.params.id).populate('medicoId').populate('pacienteId');
    return res.status(200).json(citadetail);      
    } catch (err) {
    return res.status(500).json(err);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await Citas.findByIdAndDelete(req.params.id);
    return res.status(200).json('Cita Borrada');
  } catch (err) {
    next(err);
  }
});

module.exports = router;
