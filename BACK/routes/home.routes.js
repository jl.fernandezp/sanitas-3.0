const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.status(200).render('home', { head_title: 'SaniAPP',title:'Su salud es lo primero, Gestión de citas Medicas', user:req.user});
});
module.exports = router;
