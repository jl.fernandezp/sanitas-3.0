/* eslint-disable new-cap */
const express = require('express');
const passport = require('passport');
const router = express.Router();
const Medico = require('../models/Medico');


// Para devolver formato json return res.status(200).json(publi);
router.get('/', async (req, res) => {
  const especialidad = req.query.especialidad;
  try {
    let medicos = []
    if (especialidad?.length){
      medicos = await Medico.find({especialidad:req.query.especialidad});
   }
   else{
    medicos = await Medico.find();
   }
   return res.status(200).json(medicos);
  } catch (err) {
    return res.status(500).json(err);
  }
});
router.get('/especialidades', async (req, res) => {
  try {
    const especialidades = await Medico.schema.path('especialidad').enumValues;
      return res.status(200).json(especialidades);
  } catch (err) {
    return res.status(500).json(err);
  }
});
router.post('/', async (req, res) => {
  try {
    const newMedico = new Medico({
      nombre: req.body.nombre,
      especialidad: req.body.especialidad,
    });
    const createdMedico = await newMedico.save();
    return res.status(200).json(createdMedico);

  } catch (err) {
    return res.status(500).json(err);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    const updatedMedico = await Medico.findByIdAndUpdate(
      req.params.id, // La id para encontrar el documento a actualizar
      { nombre: req.body.nombre , especialidad:req.body.especialidad }, // Campos que actualizaremos
      { new: true } // Retorna el nuevo elemento modificado
    );
    return res.status(200).json(updatedMedico);
  } catch (err) {
    return res.status(500).json(err);
  }
});
module.exports = router;


