/* eslint-disable new-cap */
const express = require('express');
const passport = require('passport');
const router = express.Router();
const User = require('../models/User');
const fileMiddlewares = require('../middlewares/file.middleware');

// Para devolver formato json return res.status(200).json(publi);
router.get('/', async (req, res) => {
  try {
    const users = await User.find();
      return res.status(200).json(users);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.post('/', async (req, res) => {
  try {
    const newUser = new User({
      dni: req.body.dni,
      email: req.body.email,
      password: req.body.password,
      fullname: req.body.fullname,
      fecha:req.body.fecha,
      direccion: req.body.direccion,
    });
    const createdUser = await newUser.save();
    return res.status(200).json(createdUser);
    //Lanzamos la opciones para mandar un correo al usuario que se ha creado 
    

  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const userdetail = await User.findById(req.params.id);
    return res.status(200).json(userdetail);      
    } catch (err) {
    return res.status(404).json(err);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    const updatedUser = await User.findByIdAndUpdate(
      req.params.id, // La id para encontrar el documento a actualizar
      { email: req.body.email , password:req.body.password , fullname: req.body.fullname,direccion: req.body.direccion }, // Campos que actualizaremos
      { new: true } // Retorna el nuevo elemento modificado
    );
    return res.status(201).json(updatedUser);
  } catch (err) {
    return res.status(404).json(err);
  }
});

router.post('/register', (req, res, next) => {
  passport.authenticate('register', (error, user) => {
    if (error) {
      const data = {'error':`${error}`};
      return res.status(400).json(data);
    }
    req.logIn(user, (err) => {
      // Si hay un error registrando al usuario, resolvemos el controlador
      if (err) {
        const data = {'error':`${err}`};
        return res.status(400).json(data);
      }
      return res.status(201).json(user);
    });
  })(req, res, next);
});

router.post('/login', (req, res, next) => {
  passport.authenticate('login', (error, user) => {
    if (error) {
      const data = {'error':`${error}`};
      return res.status(204).json(data);
    }
    req.logIn(user, (err) => {
      // Si hay un error logeando al usuario, resolvemos el controlador
      if (err) {
        const data = {'error':`${err}`};
        return res.status(400).json(data);
      }
      // Si no hay error, Retornomos un status 200 - OK
      return res.status(200).json(user);
    });
  })(req, res, next);
});

router.post('/logout', (req, res, next) => {
  if (req.user) {
    // Destruimos el objeto req.user para este usuario
    req.logout();

    req.session.destroy(() => {
      // Eliminamos la cookie de sesión al cancelar la sesión
      res.clearCookie('connect.sid');
      // Retornomos un status 200 - OK
      return res.sendStatus(200);
    });
  } else {
    return res.sendStatus(304); // Si no hay usuario, no habremos cambiado nada
  }
});
module.exports = router;


