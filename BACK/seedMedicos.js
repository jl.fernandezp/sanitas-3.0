require('dotenv').config();
const mongoose = require('mongoose');
const Medico =require('./models/Medico');
const { db_conection } =require('./config/db');

const medicos = [
  {
    nombre: "Juan Pérez",
    especialidad :"Cardiología",
  },
  {
    nombre: "Antonio Martín",
    especialidad :"Pediatría",
  },
  {
    nombre: "Marta Cano",
    especialidad :"Cardiología",
  },
  {
    nombre: "Rodrigo Jiménez",
    especialidad :"Fisioterapia",
  },
  {
    nombre: "Rocio Puertas",
    especialidad :"Ginecología",
  },
  {
    nombre: "Andrea Calvo",
    especialidad :"Medicina general",
  },
  {
    nombre: "Mario Polo",
    especialidad :"Ginecología",
  },
  {
    nombre: "Jairo García",
    especialidad :"Oftalmología",
  }
];

// Completa el código usando mongoose y el array para guardar la seed en nuestra DB
const medicosDocuments = medicos.map((medico) =>new Medico(medico));

db_conection
  .then(async () => {
    // Buscamos los usuarios creados en el db
    const allMedicos = await Medico.countDocuments();
    // Si tenemos artistas vaciamos la db
    if (allMedicos) {
      await Medico.deleteMany();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    // Cuando tengamos vacia la db creamos los usuarios definidos en users
    await Medico.insertMany(medicosDocuments);
  })
  .catch((err) => console.log(`Error creating data: ${err}`))
// Por último nos desconectaremos de la DB.
  .finally(() => mongoose.disconnect());
