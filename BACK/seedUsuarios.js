require('dotenv').config();
const mongoose = require('mongoose');
const User =require('./models/User');
const { db_conection } =require('./config/db');

const users = [
  {
    dni: "03128022E",
    email: "marconchaga@gmail.com",
    fecha: "1982, 06, 28",
    password: "123456",
    fullname: "Mar Concha", //revisar requerido
    direccion: "Calle Ejemplo",
    codigopostal: "19171",
  }
];

// Completa el código usando mongoose y el array para guardar la seed en nuestra DB
const userDocuments = users.map((user) =>new User(user));

db_conection
  .then(async () => {
    // Buscamos los usuarios creados en el db
    const allUsers = await User.countDocuments();
    // Si tenemos artistas vaciamos la db
    if (allUsers) {
      await User.deleteMany();
    }
  })
  .catch((err) => console.log(`Error deleting data: ${err}`))
  .then(async () => {
    // Cuando tengamos vacia la db creamos los usuarios definidos en users
    await User.insertMany(userDocuments);
  })
  .catch((err) => console.log(`Error creating data: ${err}`))
// Por último nos desconectaremos de la DB.
  .finally(() => mongoose.disconnect());
