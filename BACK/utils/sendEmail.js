require('dotenv').config()
const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const from = process.env.SENDGRID_API_FROM;

const sendEmail = (to,subject,content) =>{
  console.log(`El emial del usuario es:${to}`);
  const msg = {
    //to: to, 
    to: 'marconchaga@gmail.com',
    from: from, 
    subject: subject,
    html: content,
  }
  sgMail
    .send(msg)
    .then(() => {
      console.log('Email sent')
    })
    .catch((error) => {
      console.error(error);
    })
};

//sendEmail('marconchaga@gmail.com','Alta Usuario- MediCal','<strong>Gracias por Registrarse en MeciCal</strong>');

module.exports = sendEmail;
