import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import routes from "./config/routes";

import AuthProvider from "./components/AuthProvider";
import NoUserRoute from "./components/RouteNoUser";
import SecureRoute from "./components/RouteSecure";
import Appbar from "./components/Appbar";
import Home from "./components/Home";
import Registro from "./components/Registro";
import Login from "./components/Login";
import Password from "./components/Password";
import CitasList from "./components/CitasList";
import CitasDetail from "./components/CitasDetail";
import CitasCreate from "./components/CitasCreate";
import UserDetail from "./components/UserDetail";
import Footer from "./components/Footer";

import "./App.css";
//import theme from "./themes/default";

const App = () => {
  return (
    <AuthProvider>
      <div className="App">
        <Router>
          <Appbar />
          <Switch>
            <NoUserRoute exact path={routes.home}>
              <Home />
            </NoUserRoute>
            <NoUserRoute exact path={routes.register}>
              <Registro />
            </NoUserRoute>
            <NoUserRoute exact path={routes.password}>
              <Password />
            </NoUserRoute>
            <NoUserRoute exact path={routes.login}>
              <Login />
            </NoUserRoute>
            <SecureRoute exact path={routes.citas}>
              <CitasList />
            </SecureRoute>
            <SecureRoute exact path={routes.citasCreate}>
              <CitasCreate />
            </SecureRoute>
            <SecureRoute exact path={routes.citasDetail}>
              <CitasDetail />
            </SecureRoute>
            
            <SecureRoute exact path={routes.userDetail}>
              <UserDetail />
            </SecureRoute>
            <Redirect to={routes.home} />
          </Switch>
        </Router>
        <Footer />
      </div>
    </AuthProvider>
  );
};
export default App;