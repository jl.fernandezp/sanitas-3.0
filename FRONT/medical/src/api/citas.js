const baseUrl = "http://localhost:3000/citas";


const getCitas = async (usr) => {
  const response = await fetch(`${baseUrl}?usr=${usr}`)
  const jsonResponse = await response.json();
  return jsonResponse;
}

const getCita = async (id) => {
  const response = await fetch(`${baseUrl}/${id}`)
  const jsonResponse = await response.json();
  if (!response.ok){
    throw new Error(jsonResponse.error);
  }
  return jsonResponse;
};

export {
  getCitas,
  getCita,
};

  
