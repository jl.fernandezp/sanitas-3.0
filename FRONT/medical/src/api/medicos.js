const baseUrl = "http://localhost:3000/medicos";

const getMedicos = async (especialidad) => {
  const response = await fetch(`${baseUrl}?especialidad=${especialidad}`);
  const jsonResponse = await response.json();
  if (!response.ok) {
    throw new Error(jsonResponse.error);
  }
  return jsonResponse;
};

const getEspecialidades = async () => {
  const response = await fetch(`${baseUrl}/especialidades`);
  const jsonResponse = await response.json();
  if (!response.ok) {
    throw new Error(jsonResponse.error);
  }
  return jsonResponse;
};

export  {
  getMedicos,
  getEspecialidades,
};


