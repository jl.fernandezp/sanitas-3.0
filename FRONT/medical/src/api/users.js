const baseUrl = "http://localhost:3000/user";

const register = async (userData) => {
  const response = await fetch(`${baseUrl}/register`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userData),
  });
  const jsonResponse = await response.json();
  if (!response.ok) {
    throw new Error(jsonResponse.error);
  }
  return jsonResponse;
};

const login = async (userData) => {
  const response = await fetch(`${baseUrl}/login`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userData),
  });
  const jsonResponse = await response.json();
  if (!response.ok) {
    throw new Error(jsonResponse.error);
  }
  return jsonResponse;
};
const getUser = async (id) => {
  const response = await fetch(`${baseUrl}/${id}`);
  const jsonResponse = await response.json();
  if (!response.ok) {
    throw new Error(jsonResponse.error);
  }
  return jsonResponse;
};
const modUser = async (id,userData) => {
  const response = await fetch(`${baseUrl}/${id}`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userData),
  });
  const jsonResponse = await response.json();
  if (!response.ok) {
    throw new Error(jsonResponse.error);
  }
  return jsonResponse;
};
const users = {
  register,
  login,
  getUser,
  modUser,
};

export default users;
