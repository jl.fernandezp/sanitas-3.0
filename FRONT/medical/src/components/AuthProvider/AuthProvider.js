import React, { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

const AuthContext = createContext();

const AuthProvider = ({ children }) => {
    const [userLogged, setUserLogged] = useState();
    const resetUser = () => {
        setUserLogged();
        console.log('User Logged Out');
    };
    const setUser = (user) => {
        setUserLogged(user);
    };
    return (
        <AuthContext.Provider value={{ userLogged, setUser, resetUser }}>
            {children}
        </AuthContext.Provider>
    );
};
AuthProvider.propTypes = {
    children: PropTypes.node,
};
AuthProvider.defaultProps = {
    children: null,
};

const useAuth = () => {
    const { userLogged, setUser, resetUser } = useContext(AuthContext);
    return { userLogged, setUser, resetUser };
}

export { useAuth };
export default AuthProvider;
