import React, {useState} from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import CitasCreateMedico from './CitasCreateMedico';
import CitasCreateEspecialidad from './CitasCreateEspecialidad';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    backButton: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
  }),
);

function getSteps() {
  return ['Seleccione Especialidad', 'Seleccione Medico', 'Seleccione Fecha y Hora'];
}

function getStepContent(stepIndex,callback,form) {
  switch (stepIndex) {
    case 0:
      return (<CitasCreateEspecialidad especialidad={form.especialidad} callback={callback}/>);
    case 1:
      return (<CitasCreateMedico especialidad={form.especialidad} medico={form.medico} callback={callback}/>);;
    case 2:
      return 'Formulario fecha';
    case 3:
        return 'Confirm';
    case 4:
        return 'Success';
    default:
      return 'Unknown stepIndex';
  }
}

export default function CitasCreate() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  const [ form, setForm ] = useState({medico: '', especialidad: '', fecha:""});


  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };
  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };
  const handleReset = () => {
    setActiveStep(0);
    setForm({medico: '', especialidad: '', fecha:""})
  };
  const handleChangeInput = (event) => {
    console.log("Habla el padre en nombre de ",event.target.name,event.target.value)
    setForm({...form, [event.target.name]: event.target.value,})

  };
  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>All steps completed</Typography>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            <Typography className={classes.instructions}>{getStepContent(activeStep,handleChangeInput,form)}</Typography>
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                Anterior
              </Button>
              <Button variant="contained" color="primary" onClick={handleNext}>
                {activeStep === steps.length - 1 ? 'Terminado' : 'Siguiente'}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}