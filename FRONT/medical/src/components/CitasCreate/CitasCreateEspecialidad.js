import React, { useState, useEffect } from "react";
import { getEspecialidades } from "../../api/medicos";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

function CitasCreateEspecialidad(props) {
  const [open, setOpen] = useState(false);
  const [especialidad, setEspecialidad] = useState(props.especialidad);
  const [especialidades, setEspecialidades] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const callback = props.callback;

  useEffect(() => {
    setIsLoading(true);
    getEspecialidades().then((data) => {
      setEspecialidades(data);
      setIsLoading(false);
    });
    return () => {
      setEspecialidades([]);
    };
  }, [props.especialidad]);
  console.log({ especialidades });
  const handleChange = (event) => {
    setEspecialidad(event.target.value);
    callback(event);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = () => {
    setOpen(true);
  };
  return (
    <MuiThemeProvider>
      <React.Fragment>
        {isLoading ? (
          <h2>Cargando ...</h2>
        ) : (
          <Select
            margin="normal"
            required
            id="especialidad"
            label="Especialidad"
            name="especialidad"
            autoComplete="email"
            autoFocus
            open={open}
            onClose={handleClose}
            onOpen={handleOpen}
            value={especialidad}
            onChange={handleChange}
          >
            {especialidades.map((especialidad) => (
              <MenuItem value={especialidad}>{especialidad}</MenuItem>
            ))}
          </Select>
        )}
      </React.Fragment>
    </MuiThemeProvider>
  );
}
export default CitasCreateEspecialidad;
