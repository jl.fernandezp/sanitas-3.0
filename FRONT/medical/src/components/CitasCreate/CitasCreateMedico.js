import React, { useState, useEffect } from "react";
import { getMedicos } from "../../api/medicos";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

function CitasCreateMedico(props) {
  const [open, setOpen] = useState(false);
  const [medico, setMedico] = useState(props.medico);
  const [medicos, setMedicos] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const callback = props.callback;

  useEffect(() => {
    setIsLoading(true);
    getMedicos(props.especialidad).then((data) => {
      setMedicos(data);
      setIsLoading(false);
    });
    return () => {
      setMedicos([]);
    };
  }, [props.especialidad]);
  console.log({ medicos });
  const handleChange = (event) => {
    setMedico(event.target.value);
    callback(event);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = () => {
    setOpen(true);
  };
  return (
    <MuiThemeProvider>
      <React.Fragment>
        {isLoading ? (
          <h2>Cargando ...</h2>
        ) : (
          <Select
            margin="normal"
            required
            id="medico"
            label="Medico"
            name="medico"
            autoComplete="medico"
            autoFocus
            open={open}
            onClose={handleClose}
            onOpen={handleOpen}
            value={medico}
            onChange={handleChange}
          >
            {medicos.map((medico) => (
              <MenuItem value={medico._id} key={medico._id}>
                {medico.nombre}
              </MenuItem>
            ))}
          </Select>
        )}
      </React.Fragment>
    </MuiThemeProvider>
  );
}
export default CitasCreateMedico;
