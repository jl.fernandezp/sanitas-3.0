import React, { useState, useEffect } from "react";
import routes from "../../config/routes";
import { getCita } from "../../api/citas";
import { Link, useParams, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    maxWidth: 275,
    display: "inline-block",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

const CitasDetail = () => {
  const params = useParams();
  const [cita, setCita] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const classes = useStyles();
  const history = useHistory();
  const handleCitasList = () => {history.push(routes.citas);
 };

  useEffect(() => {
    setIsLoading(true);
    getCita(params.id).then((data) => {
      setCita(data);
      setIsLoading(false);
    });
    return () => {
      setCita({});
    };
  }, [params.id]);
  console.log(cita);
  return (
    <div style={{ marginTop: "50px" }}>
      {isLoading ? <div>Cargando ...</div> : null}
      {cita ? (
        <Card className={classes.root}>
          <CardContent>
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
            >
              {cita.medicoId?.especialidad}
            </Typography>
            <Typography variant="h5" component="h2">
              {cita.medicoId?.nombre}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              {cita.fecha}
            </Typography>
            <Typography variant="body2" component="p">
              {cita.tipo}
            </Typography>
          </CardContent>
          <CardActions>
            <Button size="small">Learn More</Button>
          </CardActions>
        </Card>
      ) : (
        <div>Cita no encontrado</div>
      )}
      <div>
        <button onClick={handleCitasList}>Cerrar detalle cita</button>
      </div>
    </div>
  );
};

export default CitasDetail;

