import React, { useState, useEffect } from "react";
import { getCitas } from "../../api/citas";
import routes from "../../config/routes";
import { generatePath, Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import { useAuth } from "../../components/AuthProvider";

const useStyles = makeStyles({
  root: {
    "& > *": {
      borderStyle: "solid",
      border: 3,
    },
  },
});
const Citas = () => {
  const [citas, setCitas] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { userLogged } = useAuth();

  const classes = useStyles();
  useEffect(() => {
    setIsLoading(true);
    getCitas(userLogged).then((data) => {
      setCitas(data);
      setIsLoading(false);
    });
    return () => {
      setCitas([]);
    };
  }, [userLogged]);
  return (
    <div>
      <h1>Listado de Citas</h1>
      {isLoading ? (
        <h2>Cargando ...</h2>
      ) : (
        <div style={{ marginLeft: "100px", marginRight: "100px" }}>
          {userLogged}
          <div style={{ marginTop: "30px"}}>
          <button>
            <Link to={routes.citasCreate}>Crear Cita</Link>
          </button>
          </div>
          <div style={{ marginTop: "50px" }}>
            <TableContainer component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow className={classes.root}>
                    <TableCell align="center" rowSpan={2}>
                      Medico
                    </TableCell>
                    <TableCell colSpan={4} align="center">
                      Citas
                    </TableCell>
                  </TableRow>
                  <TableRow className={classes.root}>
                    <TableCell align="right">especialidad</TableCell>
                    <TableCell align="right">Fecha&nbsp;</TableCell>
                    <TableCell align="right">Hora&nbsp;</TableCell>
                    <TableCell align="right">Tipo&nbsp;</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {citas.map((cita) => (
                    <TableRow className={classes.root} key={cita._id}>
                      <TableCell align="right">
                        <Link
                          to={generatePath(routes.citasDetail, {
                            id: cita._id,
                          })}
                        >
                          {cita.medicoId?.nombre}
                        </Link>
                      </TableCell>
                      <TableCell align="right">
                        {cita.medicoId?.especialidad}
                      </TableCell>
                      <TableCell align="right">{cita.fecha}</TableCell>
                      <TableCell align="right">{cita.horaini}</TableCell>
                      <TableCell align="right">{cita.tipo}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </div>
      )}
    </div>
  );
};

export default Citas;
