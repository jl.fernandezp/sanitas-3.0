import React from 'react';
import {useAuth} from '../../components/AuthProvider';

const Home = () => {
  const {userLogged} = useAuth();
    return(
      <div className="home">
        {userLogged ? <h3>Usuario: {userLogged}</h3> : <h3>Bienvenidos</h3>}
        <nav className="home__nav">
        </nav>
      </div>
    )
}

export default Home;