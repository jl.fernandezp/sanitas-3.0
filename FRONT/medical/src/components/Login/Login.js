import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import users from '../../api/users';
import routes from  '../../config/routes';
import { Link, useHistory } from 'react-router-dom';
import { useAuth } from '../../components/AuthProvider';



const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Login() {
  //definimos los estados 
  const [ form, setForm ] = useState({email: '', password: '', dni:"", fullname:""});
  const [ error, setError ] = useState('');
  const history = useHistory();
  const { setUser } = useAuth();

  const classes = useStyles();
  const handleFormSubmit= async (event) =>{
    event.preventDefault();
    try {
      const data = await users.login(form);
      const userId = data._id;
      setUser(userId);
      history.push(routes.citas);
    } catch(error){
      const errordata =JSON.stringify(error)
      console.log("Entra por el catch",errordata);
      setError(errordata);
    } 
  };
  const handleChangeInput = (event) => {
    setForm({...form, [event.target.name]: event.target.value,})

  };
  return (    
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <form className={classes.form} onSubmit={handleFormSubmit}>

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email"
            name="email"
            autoComplete="email"
            autoFocus
            type="email"
            onChange={handleChangeInput}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={handleChangeInput}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Login
          </Button>
          <Grid container>
            <Grid item xs>
              <Link to={routes.password} variant="body2">
                ¿Has olvidado tu contraseña?
              </Link>
            </Grid>
            <Grid item>
              <Link to={routes.register} variant="body2">
                No tienes cuenta? Regístrate
              </Link>
            </Grid>
          </Grid>
        </form>
        {error && JSON.stringify(error) }
      </div>
     </Container>
  );
}
