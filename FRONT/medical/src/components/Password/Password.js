import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import users from '../../api/users';
import routes from  '../../config/routes';
import { Link, useHistory } from 'react-router-dom';



const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function RecuperarPsw(getId) {
  //definimos los estados 
  const [ form, setForm ] = useState({email: ''});
  const [ error, setError ] = useState('');

  let history = useHistory();

  const classes = useStyles();
  const handleFormSubmit= async (event) =>{
    event.preventDefault();
    try {
      const data = await users.login(form);
      console.log(data);
      const userId = data._id;
      //history.push(routes.citas);
      history.push(routes.login);
      console.log('Usuario logeado: ', userId);
    } catch(error){
      console.log(`Entra por el catch ${error}`);
      setError({error});
    } 
  };
  const handleChangeInput = (event) => {
    setForm({...form, [event.target.name]: event.target.value,})

  };
  return (    
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Recuperar contraseña
        </Typography>
        <form className={classes.form} onSubmit={handleFormSubmit}>

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email"
            name="email"
            autoComplete="email"
            autoFocus
            type="email"
            onChange={handleChangeInput}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Recuperar
          </Button>
          <Grid container>
            <Grid item xs>
              <Link onClick={()=>{}} to={routes.login} variant="body2">
              {"Ya tienes cuenta? Haz Login"}
              </Link>
            </Grid>
            <Grid item>
              <Link onClick={()=>{}} to={routes.register} variant="body2">
                {"No tienes cuenta? Regístrate"}
              </Link>
            </Grid>
          </Grid>
        </form>
        {error && JSON.stringify(error) }
      </div>
     </Container>
  );
}
