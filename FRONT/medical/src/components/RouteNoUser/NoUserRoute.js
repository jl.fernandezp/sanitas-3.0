import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import routes from  '../../config/routes';
import {useAuth} from '../AuthProvider';

const NoUserRoute = ({...routeProps}) =>{
    const {userLogged} = useAuth();
    return !userLogged ? <Route {...routeProps} /> : <Redirect to={routes.home}/>
}

export default NoUserRoute;