import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import routes from '../../config/routes';
import {useAuth} from '../AuthProvider';

const SecureRoute = (props) =>{
    const {userLogged} = useAuth();
    const { ...routeProps } = props;
    return userLogged ? <Route {...routeProps} /> : <Redirect to={routes.home}/>
  }
export default SecureRoute;