import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import users from "../../api/users";
import routes from "../../config/routes";
import { useParams, useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const UserDetail = () => {
  const params = useParams();
  const [user, setUser] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [form, setForm] = useState({
    email: "",
    password: "",
    dni: "",
    fullname: "",
  });
  const [error, setError] = useState("");
  const classes = useStyles();
  const history = useHistory();

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    try {
      await users.modUser(params.id,form);
      history.push(routes.citas);
    } catch (error) {
      console.log(`Entra por el catch ${error}`);
      setError({ error });
    }
  };
  const handleChangeInput = (event) => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  useEffect(() => {
    setIsLoading(true);
    users.getUser(params.id).then((data) => {
      setUser(data);
      setIsLoading(false);
      setForm(data);
    });
    return () => {
      setUser({});
    };
  }, [params.id]);
  return (
    <div>
      {isLoading ? <div>Cargando ...</div> : null}
      {user ? ( 
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <div className={classes.paper}>
            <Avatar className={classes.avatar}>
              <AccountCircleIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Mis Datos {user.fullname}
            </Typography>
            <form className={classes.form} onSubmit={handleFormSubmit}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="fullname"
                label="Nombre Completo"
                name="fullname"
                autoComplete="fullname"
                autoFocus
                type="text"
                value = {form.fullname}
                onChange={handleChangeInput}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="dni"
                label="DNI"
                name="dni"
                autoComplete="dni"
                type="text"
                value = {form.dni}
                onChange={handleChangeInput}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                autoComplete="email"
                type="email"
                value = {form.email}
                onChange={handleChangeInput}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Modificar
              </Button>
            </form>
            {error && JSON.stringify(error)}
          </div>
        </Container>
      ) : (
        <div>Usuario no encontrado</div>
      )}
    </div>
  );
};
export default UserDetail;
