const home = '/';
const citas = `${home}citas`
const citasDetail = `${citas}/:id`
const citasCreate = `${citas}/create`

const register = `${home}register`
const login = `${home}login`
const logout = `${home}logout`
const password = `${home}psw`
const userDetail = `${home}user/:id`
const user = `${home}user/`

const routes = {
  home,
  citas,
  citasDetail,
  citasCreate,
  register,
  login,
  logout,
  password,
  userDetail,
  user,
}

export default routes;